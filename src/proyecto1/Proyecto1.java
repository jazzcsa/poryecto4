
package proyecto1;
import java.io.*;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import  java.util.Stack;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;


public class Proyecto1 {
    //pila
    Stack<Integer> S;
    Stack<Integer> ExP;
    Stack<Integer> S2;
    Stack<Integer> Ncercanos;
    List area = new ArrayList();
    List orden = new ArrayList();
    List orden1 = new ArrayList();
     List contenido = new ArrayList();
     boolean conectado = false;
    int tlista ;
    int caidfs = 0;
    int mvalorA = 100;
    int mvalorN = 0;
    int Aristamas_corta;
    int valn;
    int nodo_cercano ;
    int suma_kruskal = 0;
    int suma_kruskal_in =0;
    int suma_prim = 0;
    int ultimo_arista ;
    int ultimo_arista_ki = 0;
    int ultimo_arista_prim = 0;
    String cambio1 = "";
    String cambio2 = "";
    String cambio3 = "";
    // variables generales
    Random rm= new Random();
    Random peso = new Random();
    SecureRandom pr =new SecureRandom();
    int nDN ;
    int nDV ;
    double p;
    float dU ,d;
    String gP = " " ; 
    public int nodo_start;
    public int nodo_buscar;
 
      grafo gkruskal = new grafo();
      grafo gkruskal_inverso = new grafo();
      grafo gdikstra = new grafo();
      grafo gorigen = new grafo();
    int cndfsr = 0;
    int nodoa;
    
    
    // se decalran varios contructores
    

   //esta parte es para dijkstra
        int nMAgdij  ;
      
        nodo VN7[] ;
        arista AN7[] ;
        arista ANDF7[] ;
        nodo VN8[];
        arista AN8[];
        nodo VN9[];
        arista AN9[];
   //hasta aqui dijkstra 
        

   public Proyecto1()
   {  
       
   } 
   public Proyecto1(int a,int  b)      
   {
       this.nDN = a;
       this.nDV = b; 
   }
    public Proyecto1(int a,double b)       
   {
       this.nDN = a;
       this.p = b;  
   }
     public Proyecto1(int a,float b)       
   {
       this.nDN = a;
       this.dU = b;  
   }
     public Proyecto1 (int a)
     {
         this.nDN=a;
         
         
     }
             
            

//**********************************************************************
//*************************************************************************
//********************************************************************

   
   int kruskal ()
   {
       //variable para indicar el nodo de comienzo
        int na = 0;
           // variabel para indicar a que nodo se conecta el nodo de comienzo
        int nd = 0;
      
        int arista ;
        String nodor="";
        nMAgdij = (nDN *(nDN-1))/2;
        VN7 = new nodo[nDN];
        AN7  = new arista[nMAgdij];
        VN8 = new nodo[nDN];
        AN8 = new arista[nMAgdij];
        VN9 = new nodo[nDN];
        AN9  = new arista[nMAgdij];
        
        ANDF7  = new arista[nMAgdij];

// esto es para imprimir *********************************************************
         
         File archivo ;
         File archivo2;
         PrintWriter escribir ;
         PrintWriter esc;
        
         try {
        archivo = new File("kruskal.gv"); 
        archivo2 = new File("gorigen.gv");
        
        if (archivo.createNewFile())
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }
 //hastaaqui se contruye el archivo**************************************************
        // se contruyen todos los nodos
       for(int i = 0; i < nDN ; i++)
       { 
           VN7[i] = new nodo (i+1);
           VN8[i] = new nodo(i+1);
            VN9[i] = new nodo(i+1);
           VN7[i].dentro.add(1+i);
           VN8[i].dentro.add(1+i);
           VN9[i].dentro.add(1+i);
           
           
       }  
       // se calcula el numero maximo de aristas posibles
      for(int i =0;i<nMAgdij;i++)
      {
          
          AN7[i] = new arista();
          AN8[i] = new arista();
         AN9[i] = new arista();
      }
       // se contruyen las aristas
       for (int i=0; i<nMAgdij; i++)
       {
           
           
           // se aignan los nodos de la arista      
           AN7[i].n1 = VN7[na].nN;
           AN7[i].n2 = VN7[nd].nN;
           AN8[i].n1 = VN8[na].nN;
           AN8[i].n2 = VN8[nd].nN;
           AN9[i].n1 = VN9[na].nN;
           AN9[i].n2 = VN9[nd].nN;
           AN7[i].peso = peso.nextInt(100);
           AN8[i].peso = AN7[i].peso;
           AN9[i].peso = AN7[i].peso;
           AN7[i].nA = i+1;
           AN8[i].nA = i+1;
           AN9[i].nA = i+1;
           // se verifica si la arista es con el mimo nodo
           if (AN7[i].n1 == AN7[i].n2)
           {
               i--;
           }
           else if(pr.nextDouble()<0.6)
           {
               
               AN7[i].existe = true;
               AN8[i].existe = true;
               AN9[i].existe = true;
           }
            for(int j = 0; j<i;j++)
           {
               if((AN7[i].n1 == AN7[j].n1 &  AN7[i].n2 == AN7[j].n2)|(AN7[i].n1 == AN7[j].n2 &  AN7[i].n2 == AN7[j].n1))
               {
                   i--;
                   AN7[i].existe = false;
                   AN8[i].existe = false;
                   AN9[i].existe = false;
               }
           }         
           if(nd<nDN-1)
           {
           nd=nd+1;              
           }
           else
           {
               nd = 0;
               na++;
           }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
          VN7[na].Used = true;
          VN7[nd].Used = true; 
          VN8[na].Used = true;
          VN8[nd].Used = true;
          VN9[na].Used = true;
          VN9[nd].Used = true;
       }
       
       
 // esta parte solo es para imprimir en pantallla*************************
 // ademas se contruye el grafo
       System.out.println("\ngraph Guil {");
       //crea grafo con aristas primero
       for(int i = 0; i<nMAgdij; i++ )
       {
           if(AN7[i].existe)
           {
               orden.add(AN7[i].peso);
               gorigen.grafo = gorigen.grafo + AN7[i].n1 + " -- " +AN7[i].n2   + ";\n";
           System.out.println(AN7[i].n1 + " -- " +AN7[i].n2 + ";" + "peso: " + AN7[i].peso);
           }
       } 
       
 Collections.sort(orden);     
 

       
for (int i =0;i < orden.size();i++)
{
    for (int j=0;j <nMAgdij;j++)
    {
        if(AN7[j].peso == (int)orden.get(i))
        {
            if (AN7[j].elegida == false && AN7[j].existe == true)
            {
                System.out.println(AN7[j].n1 + "--" +AN7[j].n2);
                if(!(VN7[AN7[j].n1-1].dentro.containsAll(VN7[AN7[j].n2-1].dentro) && VN7[AN7[j].n2-1].dentro.containsAll(VN7[AN7[j].n1-1].dentro)))
                {
                   if (VN7[AN7[j].n1-1].dentro.size()>=VN7[AN7[j].n2-1].dentro.size())
                   {
                       
                       HashSet temp = new HashSet(VN7[AN7[j].n1-1].dentro);
                       HashSet temp2 = new HashSet(VN7[AN7[j].n2-1].dentro);
                      for( Iterator it = temp.iterator(); it.hasNext();) 
                    { 
                        
                         int t = ((Number)it.next()).intValue();
                         for (Iterator it2 = temp2.iterator();it2.hasNext();)
                         {
                             int t2 =((Number)it2.next()).intValue();
                               VN7[t-1].dentro.addAll(VN7[t2-1].dentro);
                    VN7[t2-1].dentro.addAll(VN7[t-1].dentro);
                             
                         }
                    
                         
                      
	           

                    } 
                   }
                   else
                       
                   {
                       
                      HashSet temp = new HashSet(VN7[AN7[j].n2-1].dentro);
                        HashSet temp2 = new HashSet(VN7[AN7[j].n1-1].dentro);
                      for( Iterator it = temp.iterator(); it.hasNext();) 
                    { 
                         int t = ((Number)it.next()).intValue();
                           for (Iterator it2 = temp2.iterator();it2.hasNext();)
                         {
                             int t2 =((Number)it2.next()).intValue();
                               VN7[t-1].dentro.addAll(VN7[t2-1].dentro);
                    VN7[t2-1].dentro.addAll(VN7[t-1].dentro);
                             
                         }
                    
                      
	           

                    } 
                   }
                   
                    AN7[j].elegida = true;
                    suma_kruskal = suma_kruskal + AN7[j].peso;
                    ultimo_arista = AN7[j].nA;
                    
                    gkruskal.grafo = gkruskal.grafo + AN7[j].n1+" -- "+AN7[j].n2+";\n";
                    
                }
                
            }
           
        }
    }
}

 cambio1 = AN7[ultimo_arista-1].n1+" -- "+AN7[ultimo_arista-1].n2;
 gkruskal.grafo = gkruskal.grafo.replace(cambio1 + ";\n", cambio1+"("+suma_kruskal+")"+"\n");
 //gkruskal.grafo = gkruskal.grafo + AN7[ultimo_arista-1].n1+" -- "+AN7[ultimo_arista-1].n2+"("+suma_kruskal+")"+"\n";
//hasta aqui parte iterativa del grafo
   
 
       
        try {
        escribir =  new PrintWriter("kruskal.gv", "utf-8") ;
        esc = new PrintWriter("gorigen.gv","utf-8");
     
        escribir.println("\ngraph kruskal {");
        esc.println("\ngraph gorigen {");
        esc.println(gorigen.grafo);
        escribir.println(gkruskal.grafo);
        esc.println("\n}");
 
        escribir.println("\n}");
        esc.close();
        escribir.close();
  
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
 //hasta aqui el archivo del grafo****************************************
    
      return 0 ;
      
      
   }
   //hasta aqui idfs********************************************************
   
   
   
  //desde aqui krukal inverso
   
   
   
      public  void abyacentes(int g)
   {
      
       
           for(int m = 0; m<AN8.length; m++)
                {
                  if(AN8[m].existe)
                        {

                            if(VN8[g].nN == AN8[m].n1 & VN7[AN8[m].n2-1].ex == false )
                            {
                                if(!contenido.contains(AN8[m].n2-1))
                                {
                              contenido.add(AN8[m].n2-1);
                                }
                            }
                            else if(  VN8[g].nN == AN8[m].n2 & VN8[AN8[m].n1-1].ex == false)
                            {
                                if(!contenido.contains(AN8[m].n1-1))
                                {
                               contenido.add(AN8[m].n1-1);
                                }
                               
                               
                                
                            } 

                        }  
                }  
   }
   
   
    public  void conectado(int a,int b)
   {
       //System.out.println("entra");
       contenido.add(a);
       for(int j=0;j<contenido.size();j++)
       {
           //System.out.println(contenido.size());
           abyacentes((int)contenido.get(j));
           
       }
       if(contenido.contains(b))
       {
           conectado = true;
       }
       else
       {
           conectado = false;
       }
     
      
   }
   
    
   int kruskal_inverso ()
   {

// esto es para imprimir *********************************************************
         
         File archivo1 ;
         PrintWriter escribir1 ;
        
         try {
        archivo1 = new File("kruskal_inverso.gv"); 
        
        if (archivo1.createNewFile())
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }
 //hastaaqui se contruye el archivo**************************************************
       
 // esta parte solo es para imprimir en pantallla*************************
 // ademas se contruye el grafo
       //crea grafo con aristas primero
       for(int i = 0; i<nMAgdij; i++ )
       {
           
           if(AN8[i].existe)
           {
               orden1.add(AN8[i].peso);
          // System.out.println(AN8[i].n1 + " -- " +AN8[i].n2 + ";" + "peso: " + AN8[i].peso);
           }
       } 
       
 Collections.sort(orden1);
 
 Collections.reverse(orden1);
 

      // System.out.println(orden1);
for (int i =0;i < orden1.size();i++)
{
    for (int j=0;j <nMAgdij;j++)
    {
        if(AN8[j].peso == (int)orden1.get(i) && AN8[j].existe == true)
        {
            AN8[j].existe = false;
            conectado(AN8[j].n1-1,AN8[j].n2-1);
            //System.out.println("regresa");
            if(!conectado)
            {
                AN8[j].existe = true;
            }
            contenido.removeAll(contenido);
            
            
        }
    }
}
for (int k=0;k<nMAgdij;k++)
{
    if (AN8[k].existe)
    {
         suma_kruskal_in = suma_kruskal_in + AN8[k].peso;
                    ultimo_arista_ki = AN8[k].nA;
        gkruskal_inverso.grafo = gkruskal_inverso.grafo + AN8[k].n1 + " -- " +AN8[k].n2 +";\n";
    }
}
       
  cambio2 = AN8[ultimo_arista_ki-1].n1+" -- "+AN8[ultimo_arista_ki-1].n2;
gkruskal_inverso.grafo = gkruskal_inverso.grafo.replace(cambio2 + ";\n", cambio2+"("+suma_kruskal_in+")"+"\n");  
//hasta aqui parte iterativa del grafo
   
 
       
        try {
        escribir1 =  new PrintWriter("kruskal_inverso.gv", "utf-8") ;
     
        escribir1.println("\ngraph K_in {");
        escribir1.println(gkruskal_inverso.grafo); 
 
        escribir1.println("\n}");
        escribir1.close();
  
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
 //hasta aqui el archivo del grafo****************************************
    
      return 0 ;
      
      
   }
   //hasta aqui idfs********************************************************
   
   
   //funcion para ingresar abyacentes a pila
   public  void abyacentes2()
   {
      tlista = area.size();
      
      for (int f = 0;f<tlista;f++)
      {       
           valn=(int)area.get(f);
         
           for(int m = 0; m<AN9.length; m++)
                {
                  if(AN9[m].existe)
                        {

                            if(VN9[valn].nN == AN9[m].n1 & !area.contains(VN9[AN9[m].n2-1].nN-1))
                            {
                                
                                if(AN9[m].elegida == false)
                                {
                                   
                              S2.add(AN9[m].nA);
                                }
                            }
                            else if(  VN9[valn].nN == AN9[m].n2 & !area.contains(VN9[AN9[m].n1-1].nN-1))
                            {
                              
                               if(AN9[m].elegida == false )
                                {
                                  
                               S2.add(AN9[m].nA);
                                }
                               
                               
                                
                            } 

                        }  
                }    
      } 
      
   }
   
   int prim ()
   {
       String nodor="";
       //variable para indicar el nodo de comienzo

        int NE;
        int NAC;//no elegido para inicio de bfs

        int po=0;
        int distancia = 0;
         S2 = new Stack<Integer>();
        Ncercanos  = new Stack<Integer>();
        

// esto es para imprimir *********************************************************

         File archivo2 ;
        
         PrintWriter escribir2;
         try {
        archivo2 = new File("prim.gv"); 
      
        
        
        if (archivo2.createNewFile() )
        {
            System.out.println("Se ha creado exitosamente");
        }
         } catch(IOException e)
         {
             System.err.println("No se a podido crera el archivo"+ e);
         }

       

       
          
 //hasta aqui es para imprimir en pantalla********************************
    NE  = rm.nextInt(nDN);
       System.out.println(NE);
    
    NAC = 2;
   
   
    nodor = nodor + VN9[NE].nN;
  
//aqui se hace la parte iterativa del grafo
area.add(NE);
      
//aqui se hace la parte iterativa del grafo
for ( int i=0; i < nDN;i++)
{
    abyacentes2();

  
 while(!S2.empty())
 {
    
     mvalorN = S2.pop();
 
     if(AN9[mvalorN-1].peso < mvalorA && AN9[mvalorN-1].elegida == false)
             {
             
                Aristamas_corta = mvalorN;
                mvalorA = AN9[mvalorN-1].peso;
               
                 
             }
     
    
 }
   
 AN9[Aristamas_corta-1].elegida = true;
    
    
 mvalorA = 100;
   

 
 
 if (area.contains(VN9[AN9[Aristamas_corta-1].n1 - 1].nN-1) && !area.contains(VN9[AN9[Aristamas_corta-1].n2 - 1].nN-1) )
 {
 
   
 gdikstra.grafo = gdikstra.grafo + AN9[Aristamas_corta-1].n1 + " -- " +AN9[Aristamas_corta-1].n2 + ";\n" ;
 suma_prim = suma_prim + AN9[Aristamas_corta-1].peso;
                    ultimo_arista_prim = AN9[Aristamas_corta-1].nA;
                      area.add(VN9[AN9[Aristamas_corta-1].n2 - 1].nN-1);
                     
 }
 else if (area.contains(VN9[AN9[Aristamas_corta-1].n2 - 1].nN-1) && !area.contains(VN9[AN9[Aristamas_corta-1].n1 - 1].nN-1)  )
 {
    
 gdikstra.grafo = gdikstra.grafo + AN9[Aristamas_corta-1].n1 + " -- " +AN9[Aristamas_corta-1].n2 + ";\n" ;  
 suma_prim = suma_prim + AN9[Aristamas_corta-1].peso;
                    ultimo_arista_prim = AN9[Aristamas_corta-1].nA;
                    area.add(VN9[AN9[Aristamas_corta-1].n1 - 1].nN-1);
                    
 }
 /*
  if (area.contains(VN9[AN9[Aristamas_corta-1].n1 - 1].nN-1) && !area.contains(VN9[AN9[Aristamas_corta-1].n2 - 1].nN-1))
         {
             area.add(VN9[AN9[Aristamas_corta-1].n2 - 1].nN-1);
         }
 else if (area.contains(VN9[AN9[Aristamas_corta-1].n2 - 1].nN-1) && !area.contains(VN9[AN9[Aristamas_corta-1].n1 - 1].nN-1))
 {
    
     area.add(VN9[AN9[Aristamas_corta-1].n1 - 1].nN-1);
 }*/
 
 
 }

cambio3 = AN9[ultimo_arista_prim-1].n1+" -- "+AN9[ultimo_arista_prim-1].n2;
 gdikstra.grafo = gdikstra.grafo.replace(cambio3 + ";\n", cambio3+"("+suma_prim+")"+"\n");
      
// nodor = nodor + "("+distancia+")";
    //System.out.println(area); 


//hasta aqui parte iterativa del grafo
   
 
       
        try {
        escribir2 =  new PrintWriter("prim.gv", "utf-8") ;
     
        escribir2.println("\ngraph prim {");
        escribir2.println(gdikstra.grafo); 
 
        escribir2.println("\n}");
        escribir2.close();
  
         } catch(IOException e)
         {
             System.err.println("No se a podido escribir en  el archivo"+ e);
         }
 //hasta aqui el archivo del grafo****************************************
    
      return 0 ;
      
      
   }
   //hasta aqui idfs********************************************************
   
   

   
}


// desde aqui krukal inverso


